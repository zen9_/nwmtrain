<?php
session_start();

if ($_SESSION['user']) {
    header('Location: profile.php');
}
?>

    <!-- signin-->
    <form method="POST" action="authorization/handler_form/signin.php">
        <div id="range1">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="login-wr">
                            <div class="form">
                                <div class="form_text">
                                    Логин
                                </div>
                                <input type="text" name="login" placeholder="Введите логин">
                                <div class="form_text">
                                    Пароль
                                </div>
                                <input type="password" name="password" placeholder="Введите пароль">
                                <button type="submit">Войти</button>
                                <p>
                                    
                                    <br>
                                    <a href="index.php?page=register">Регистрация</a>
                                </p>
                                <?php
                                    if ($_SESSION['message_err']) {
                                        echo '<p class="msg"> ' . $_SESSION['message_err'] . ' </p>';
                                    }
                                    unset($_SESSION['message_err']);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>