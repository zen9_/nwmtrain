<?php
session_start();
if (!$_SESSION['user']) {
    header('Location: /');
}
$id_user = $_SESSION['user']['id'];
?>

    <div id="authorization">
        <form id="login">
            <table align="center">
                <tr>
                    <td colspan="2" align="center">
                        <h2>Вы вошли как пользователь</h2>

                    </td>
                </tr>
                <tr>
                    <td> Ваше ФИО</td>
                    <td style="padding-left: 25px;">
                        <p><?= $_SESSION['user']['full_name'] ?></p>
                    </td>
                </tr>
                <tr>
                    <td> Ваш адрес электронной почты</td>
                    <td style="padding-left: 25px;">
                        <p><?= $_SESSION['user']['email'] ?></p>
                    </td>
                </tr>
                
            </table>


            <table>
                <tr>
                    <td></td>
                    <td><b>Наименование</b></td>
                    <td><b>Количество</b></td>
                    <td><b>Цена за единицу</b></td>
                    <td><b>Итого</b></td>
                </tr>
                

                <?php

        //подключаемся к БД и выбираем все данные о наших товарах

                $sql_m= $link->query("SELECT * FROM `product`");
                $Sum = 0; 
                $sql_basket= $link->query("SELECT * FROM `basket`"); 
        //выбираем только те товары, чей id соответствует id товар из массива //$_SESSION['add_id'] добавленных в корзину

            if(isset($sql_basket)){
                foreach ($sql_basket as $basket){
                    if($basket['id_user']==$id_user){
                        $kol = $basket['number_product'];
                        $a = $basket['id_product'];
                        $good_m = [];
                    foreach ($sql_m as $product_m) {
                        if($product_m['id'] == $a){
                        $good_m= $product_m;
                        break;  
                        }   
                    }   
                     ?> 

                <tr>
                    <td><img width="50px" src="<?php echo $good_m['imgs']; ?>" /></td>
                    <td><?php echo $good_m['point1']."→".$good_m['point2']; ?></td>
                    <td><?php echo $kol; ?></td>

                    <td><?php echo $good_m['price'].'₽'; ?></td>
                    <td><?php echo $kol*$good_m['price'].'₽'; ?></td>
                    <td><a style="color: #8B0000" href="../tickets/ticket.php">Отправить на почту</a></td>
                    <td><a style="color: #8B0000" href="../tickets/return_ticket.php">Вернуть</a></td>
                    
                </tr>

                <?php
                $_SESSION['p1_tick']        = $good_m['point1'];
                $_SESSION['p2_tick']        = $good_m['point2'];
                $_SESSION['img_tick']       = $good_m['imgs'];
                $_SESSION['price_tick']     = $good_m['price'];
                $_SESSION['dep_time_tick']  = $good_m['departure_time'];
                ?>

<!--  Считаем итоговую сумму заказа -->
        <?php
        $Sum +=$kol*$good_m['price'];
        $i++;    
        } 
    }
}          

    // Выводим итоговую сумму заказа
        ?>
        <tr>
             <td align="right" colspan="5"><b> <?php echo 'Всего: '.$Sum. "₽" ?></b></td>
         </tr> 
         <tr>
                    <td colspan="7" class="exit">
                        <p><a href="authorization/handler_form/logout.php" class="logout">Выход</a></p>
                    </td>
                </tr>
        </table>
        </form>


    </div>
