<?php
    session_start();
    if ($_SESSION['user']) {
        header('Location: profile.php');
    }
    $email = htmlspecialchars(trim($_POST['email'])); 
?>

    <form action="authorization/handler_form/signup.php" method="post" enctype="multipart/form-data">
        <div id="range1">
            <div class="outer">
                <div class="middle">
                    <div class="inner">
                        <div class="login-wr">
                            <div class="form">
                                <div class="form_text">
                                    ФИО
                                </div>
                                <input type="text" name="full_name" placeholder="Введите свое полное имя">
                                <div class="form_text">
                                    Логин
                                </div>
                                <div class="text-danger"><?=$_SESSION['message_login']?></div>
                                <input type="text" name="login" placeholder="Введите свой логин">
                                <div class="form_text">
                                    Почта
                                </div>
                                <div class="text-danger"><?=$_SESSION['error_email']?></div>
                                <input type="email" name="email" placeholder="Введите адрес своей почты">
                                <div class="form_text">
                                    Пароль
                                </div>
                                <div class="text-danger"><?=$_SESSION['message_password']?></div>
                                <input type="password" name="password" placeholder="Введите пароль">

                                <div class="form_text">
                                    Подтверждение пароля
                                </div>
                                <input type="password" name="password_confirm" placeholder="Подтвердите пароль">
                                <button type="submit"> Зарегистрироваться </button>
                                
                                <p>
                                    <a href="index.php?page=index_profile">Авторизация</a>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>