<?php 
session_start();
  require ("templates/based/header.php");
?>

<div id="content">

<?php

require('connect.php');

if (!isset($_SESSION['sql'])){
  $_SESSION['sql'] = "SELECT * FROM product";
}

$sql_text = $_SESSION['sql'];
$sql = $link->query($sql_text);

$page=$_GET['page'];
  if(!isset($page)){
    require('templates/pages/main.php');
  } 
  else if($page == 'shop'){
    require('templates/pages/shop.php');
  }else if($page == 'login'){
    require('authorization/index.php');
  }else if($page == 'index'){
    require('templates/pages/main.php');
  }else if($page == 'feedback'){
    require('templates/pages/feedback.php');
  }else if($page == 'shopping_cart') {
    require('templates/pages/basket.php');
  }
  
  else if($page == 'openproduct') {

    $idg = $_GET['id'];

    $good = [];

    foreach ($sql as $product) {
       if($product['id'] == $idg){
         $good = $product;
         break;
       }
     } 

    require('templates/pages/openProduct.php');

  }

  else if($page == 'product_cat'){

    $idg = $_GET['id_cat'];

    if($idg == 0){
      $_SESSION['sql'] = "SELECT * FROM product";
    }
    else{
      $_SESSION['sql'] = "SELECT * FROM product WHERE category = $idg";
    }
    $sql_text = $_SESSION['sql'];
      $sql = $link->query($sql_text);
          require('templates/pages/shop.php');
  }

  else if($page =='product_sort'){
    $idg = $_GET['sort_id'];

    if($idg == 1){
      $sql_text.=" ORDER BY departure_time";
    }
    else if($idg==2){
      $sql_text.=" ORDER BY departure_time DESC";
    }
    else if($idg == 3){
      $sql_text.= " ORDER BY price ASC"; 
    }
    else{
      $sql_text.=" ORDER BY price DESC";
    }
    $sql = $link->query($sql_text);
    require('templates/pages/shop.php');
  }
  elseif($page=='index_profile'){
      if(isset($_SESSION['user'])){
          if($_SESSION['user']['role'] == '1'){
            require ('authorization/admin.php');
          }elseif($_SESSION['user']['role'] == '0'){
            require('authorization/profile.php');
          }
      }else{
        require('authorization/index.php');
      }
  }    
  elseif($page=='register'){
    require('authorization/register.php');
  }elseif($page=='logout'){
    require('authorization/index.php');
  }elseif($page=='profile'){
    require('authorization/profile.php');
  }elseif($page=='admin'){
    require('authorization/admin.php');
  }

?>

</div>

<?php 

  require ("templates/based/footer.php");
?>