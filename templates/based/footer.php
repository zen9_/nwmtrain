<!--Модальное окно-->
<div id="openModal" class="modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title">Корзина</h3>
            <a href="#close" title="Close" class="close">×</a>
          </div>
          <div class="modal-body">
            <table>
                <tr>
                    <td></td>
                    <td><b>Наименование</b></td>
                    <td><b>Количество</b></td>
                    <td><b>Цена за единицу</b></td>
                    <td><b>Итого</b></td>
                </tr>
                

                <?php

                // $i = 0;
                // $len = count($_SESSION['add_id']);

        //подключаемся к БД и выбираем все данные о наших товарах

                $sql_m= $link->query("SELECT * FROM `product`");
                $Sum = 0;  
        //выбираем только те товары, чей id соответствует id товар из массива //$_SESSION['add_id'] добавленных в корзину

                foreach ($_SESSION['add_product'] as $key=> $value){
                    $kol = $_SESSION['add_product'][$key];
                    $a = $key;
                    $good_m = [];
                    foreach ($sql_m as $product_m) {
                        if($product_m['id'] == $a){
                        $good_m= $product_m;
                        break;  
                        }   
                    }

                     ?> 

                <tr>
                    <td><img width="50px" src="<?php echo $good_m['imgs']; ?>" /></td>
                    <td><?php echo $good_m['point1']."→".$good_m['point2']; ?></td>
                    <td><?php echo $kol; ?></td>

                    <td><?php echo $good_m['price'].'₽'; ?></td>
                    <td><?php echo $kol*$good_m['price'].'₽'; ?></td>
                    <td>
                    <form method="POST" name="deleteAction">
                        <input type="submit" value="Удалить" name="<?php echo "deleteBtn",($a) ?>" ><?php
                        if ($_REQUEST['deleteBtn'.$a]){
                            unset($_SESSION['add_product'][$a]);
                        }

                        ?>
                    </form></td>
                    
                </tr>

<!--  Считаем итоговую сумму заказа -->
        <?php
        $Sum +=$kol*$good_m['price'];
        $i++;    
        }        

    // Выводим итоговую сумму заказа
        ?>
        <tr>
          <td colspan="5"><b> <?php echo 'Всего: '.$Sum. "₽" ?></b></td>
        </tr> 
        <tr>
            <!-- <td colspan="5">
            <form action="https://merchant.web.money/lmi/payment.asp" method="POST">
              <input type="hidden" name="LMI_PAYMENT_AMOUNT" value=<?php echo substr($Sum/61.63, 0, 4); ?>>
              <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" value="0YLQtdGB0YLQvtCy0YvQuSDRgtC+0LLQsNGA">
              <input type="hidden" name="LMI_PAYEE_PURSE" value="Z085905434858">
              <input type="submit" class="wmbtn" style="font-famaly:Verdana, Helvetica, sans-serif!important;padding:0 10px;height:30px;font-size:12px!important;border:1px solid #538ec1!important;background:#a4cef4!important;color:#fff!important;" value="&#1086;&#1087;&#1083;&#1072;&#1090;&#1080;&#1090;&#1100; <?php echo substr($Sum/61.63, 0, 4); ?> $ ">
              </form>
            </td> -->
             <td align="right" colspan="5"><b><a href="add_basket.php">Оформить заказ</a></b></td>
         </tr> 
        </table>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Модальное окно-->

<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
  <!-- Section: Links  -->
  <section class="bg-light">
    <div class="container text-center text-md-start mt-5">
      <!-- Grid row -->
      <div class="row mt-3">
        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <h6 class="text-uppercase fw-bold mb-4">
            <i class="fas fa-train me-3"></i>RuTrain
          </h6>
          <p>
            Наша компания помогает найти и купить необходимые ж/д билеты.
          </p>
        </div>
        <!-- Grid column -->


        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            Полезные ссылки
          </h6>
          <p>
            <a href="index.php?page=index" class="text-reset">Главная</a>
          </p>
          <p>
            <a href="index.php?page=shop" class="text-reset">Расписание</a>
          </p>
          <p>
            <a href="index.php?page=index_profile" class="text-reset">Личный кабинет</a>
          </p>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            Контакты
          </h6>
          <p>
            <i class="fas fa-envelope me-3"></i>
            nwmtrain@gmail.com
          </p>
          <p><i class="fas fa-phone me-3"></i> +7 999 136 37 21</p>
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
    </div>
    <div class="text-center">© 2022 Copyright: <a class="text-white" href="index.php?page=index">RuTrain</a> </div> 
  </section>
  <!-- Section: Links  -->
</footer>
</body>
</html>