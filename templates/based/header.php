<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap-5.1.3-dist/css/bootstrap.css" rel="stylesheet">
    <link href="styles/site.css" rel="stylesheet" >
    <link href="styles/basket.css" rel="stylesheet" >
    <script src="scripts/jquery.js"></script>
    <script src="scripts/site.js"></script>
    <script src="scripts/basket.js"></script>
    <script src="bootstrap-5.1.3-dist/js/bootstrap.bundle.js"></script>
    <link href="https://fonts.gstatic.com" rel="preconnect" >
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" rel="stylesheet" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <title>RuTrain</title>
</head>
<body>
    
<nav class="navbar navbar-expand-lg navbar-dark bg-light" aria-label="Ninth navbar example">
    <div class="container-xl ">
    <div id="logo"></div>
    <a class="navbar-brand" href="index.php?page=index">RuTrain</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbarsExample07XL">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
            <a class="nav-link text-white" href="index.php?page=index">Главная</a>
            </li>
            <li class="nav-item">
            <a class="nav-link text-white" href="index.php?page=shop">Расписание</a>
            </li>
            <li class="nav-item">
            <a class="nav-link text-white" href="index.php?page=feedback">Обратная связь</a>
            </li>
        </ul>
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
            <a class="nav-link text-white" href="index.php?page=index_profile">Личный кабинет</a>
            <a href="#openModal"> <img id="basket" src="images/basket.png" alt=""></a>
            </li>
        </ul>
        </div>
    </div>
</nav>

