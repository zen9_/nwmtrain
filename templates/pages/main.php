<div class="row mt-20">
  <div class="col">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Мой личный кабинет</h5>
        <p class="card-text">Для входа в личный кабинет необходимо авторизоваться</p>
        <br>
        <a href="index.php?page=register" class="btn btn-danger">Регистрация</a>
        <a href="index.php?page=index_profile" class="btn btn-danger">Вход</a>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Расписание</h5>
        <p class="card-text">В расписании можно найти интересующие вас рейсы</p>
        <br>
        <a href="index.php?page=shop" class="btn btn-danger">Найти билет</a>
      </div>
    </div>
  </div>
</div>


  