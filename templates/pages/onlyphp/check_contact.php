<?php
    session_start();

    //unset($_SESSION['user_name']);
    unset($_SESSION['email']);
    unset($_SESSION['subject']);
    unset($_SESSION['message']);

    //unset($_SESSION['error_username']);
    unset($_SESSION['error_email']);
    unset($_SESSION['error_subject']);
    unset($_SESSION['error_message']);

    unset($_SESSION['success_mail']);

    function redirect() {
        header('Location: ../../../index.php?page=feedback');
        exit();
    }

    //$user_name  = htmlspecialchars(trim($_POST['username']));
    $from       = htmlspecialchars(trim($_POST['email']));
    $subject    = htmlspecialchars(trim($_POST['subject']));
    $message    = htmlspecialchars(trim($_POST['message']));

    //$_SESSION['user_name']  = $user_name;
    $_SESSION['email']      = $from;
    $_SESSION['subject']    = $subject;
    $_SESSION['message']    = $message;

    // if(strlen($user_name) <= 1) {
    //     $_SESSION['error_username'] = "Введите корректное имя";
    //     redirect();
    // }
    /*else*/ if(strlen($from) < 4 || strpos($from, "@") == false) {
        $_SESSION['error_email']    = "Неккоректный email";
        redirect();
    }
    else if(strlen($subject) <= 4) {
        $_SESSION['error_subject']  = "Тема сообщение не менее 5 символов";
        redirect();
    }
    else if(strlen($message) <= 10) {
        $_SESSION['error_message']  = "Сообщение не менее 15 символов";
        redirect();
    }
    else {
        $subject = "=?utf-8?B?".base64_encode($subject)."?=";
        $headers = "From: $from\r\nReply-to: $from\r\nContent-type:text/plain; charset=utf-8\r\n";
        mail("nwmtrain@gmail.com", $subject, "Сообщение: $message", $headers);
        $_SESSION['success_mail'] = "Сообщение отправлено";
        redirect();
    }
?>