<div id="openedProduct-img">

<img src="<?php echo $good['imgs']; ?>" />

    </div>
    <div id="openedProduct-content">
        <h1 id="openedProduct-name">
            <div class="opPoints">
                    
                    <?php echo $good['point1']; ?>
                    →
                    <?php echo $good['point2']; ?>
            </div> 
        </h1>
            <div class="opDate">
                Даты отбытия/прибытия:
                <?php echo $good['departure_date']; ?>
                →
                <?php echo $good['arrival_date']; ?>
            </div>
            <div class="opTime">
                Время отбытия/прибытия:
                <?php echo $good['departure_time']; ?>
                →
                <?php echo $good['arrival_time']; ?>
            </div>                          
        
        <div id="openedProduct-desc">
            <div class="opStations">
                Станции:
                <?php echo $good['station1']; ?>
                →
                <?php echo $good['station2']; ?>
            </div>
        <div class="opTrain">
            Поезд:
            <?php echo $good['train_type']; ?>
        </div>    
        </div>
        <div id="openedProduct-price">
            Цена:  <?php echo $good['price'].'₽'; ?>
        </div>

        <!--Счетчик товаров-->

        <form id="form1" name="form1" action="add_card.php" method="post">

        <div class="input-group quantity-goods">
            <input type="button" value="-" id="button_minus">
            <input type="number" name="quantity" step="1" min="1" max="10" value="1" id="num_count" title="qty">
            <input type="button" value="+" id="button_plus">
        </div>
        
        <!-- начало невидимой части формы -->
        <input type="hidden"  name="product_id" value="<?php echo $good['id']?>" />
        
        <!-- конец невидимой части формы -->
            
        <input class="shopUnitMore" type="submit" value="В корзину" name="submit">

        </form>


        <!-- обработчик событий для счетчика количества товаров -->

    <script>
        var numCount = document.getElementById('num_count');
        var plusBtn = document.getElementById('button_plus');
        var minusBtn = document.getElementById('button_minus');
        plusBtn.onclick = function() {
            var qty = parseInt(numCount.value);
            qty = qty + 1;
            numCount.value = qty;
            }
        minusBtn.onclick = function() {
            var qty = parseInt(numCount.value);
            if(qty>1){
            qty = qty - 1;
            }
            numCount.value = qty;
            }
    </script>
</div>