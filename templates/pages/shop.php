<div id="contentShedule">
<!-- Сортировка -->
<div class="sort">
<h3>Фильтр</h3>
    <div class="sortMenu">
            <select onchange="location=value">
                <option value="" selected="true" disabled="disabled">Фильтр по времени отправки</option>
                <option value="index.php?page=product_sort&sort_id=1">Раньше</option>
                <option value="index.php?page=product_sort&sort_id=2">Позже</option>
            </select>
            <br/>
            <select class="sortMenuSelect" onchange="location=value">
                <option value="" selected="true" disabled="disabled">Фильтр по цене</option>
                <option value="index.php?page=product_sort&sort_id=3">По возрастанию</option>
                <option value="index.php?page=product_sort&sort_id=4">По убыванию</option>
            </select>
    </div>
    <nav class="link">
        <h3>Тип поезда</h3>
        <ul>
            <?php
            $sql_cat = $link->query("SELECT * FROM category");
            foreach ($sql_cat as $cat):
            ?>
            <li><a href="index.php?page=product_cat&id_cat=<?php echo $cat['id_category'];?>"><?php echo $cat['name']?> </a></li>
            <?php endforeach;?>
            <li><a href="index.php?page=product_cat&id_cat=0">Все </a></li>
        </ul>

    </nav>
</div>
<!--Меню-->
<div class="mainBlockCatalog">

    

    <div class="catalogList">
        <?php 
            foreach ($sql as $good):

        ?>
            <div class="shopUnit">
                <!-- Изображения 550x550px -->
                <!-- <img src="<?php echo $good['imgs']; ?>" /> -->  
                <div class="shopUnitInfo">
                    <div class="unitPoints">
                        <?php echo $good['point1']; ?>
                        →
                        <?php echo $good['point2']; ?>
                    </div>
                    <div class="unitDate">
                        <?php echo $good['departure_date']; ?>
                        →
                        <?php echo $good['arrival_date']; ?>
                    </div>
                    <div class="unitTime">
                        <?php echo $good['departure_time']; ?>
                        →
                        <?php echo $good['arrival_time']; ?>
                    </div>      
                </div>
                <div class="shopUnitShortDesc">
                    <?php echo $good['discription']; ?>
                </div>
                <div class="shopUnitPrice">
                    Цена: <?php echo $good['price'] . '₽'; ?>
                </div>
                <a href="index.php?page=openproduct&id=<?php echo $good['id']; ?>" class="shopUnitMore">
                    Подробнее
                </a>
    </div>
        <?php endforeach;?>

    </div>
</div>