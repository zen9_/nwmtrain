<?php 

    session_start();

    $sendEmail = $_SESSION['user']['email'];
    $sendName = $_SESSION['user']['full_name'];


    //генерация уникального имени для билета
    $nameString = '0123456789abcdefghijklmnopqrstuvwxyz';
    $genTicketName = substr(str_shuffle($nameString), 0, 6);
    $ticketName = 'ticket-'.$genTicketName.'.pdf';
    

$html ='
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    html{
        font-family: DejaVu Sans, sans-serif;
    }
    </style>
    <title>Document</title>
</head>
<body>
    <div style="display: flex;">
        <h1>RuTrain</h1>
    </div>
    <h2>Номер электронного билета: #'    .$genTicketName.'</h2>
    <h3>ФИО: '                           .$sendName.'</h3>
    <h3>Путь: '                          .$_SESSION['p1_tick'].'→'.$_SESSION['p2_tick'].'</h3>
    
</body>
</html>'
?>

<?php

    require_once '../vendor/autoload.php';

    use Dompdf\Dompdf;
    use PHPMailer\PHPMailer\PHPMailer;
    
    // instantiate and use the dompdf class
    $dompdf = new Dompdf($options);
    $dompdf->loadHtml($html);

    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'landscape');

    // Render the HTML as PDF
    $dompdf->render();
    // Output the generated PDF to Browser
    
    $pdf = $dompdf->output();

    file_put_contents($ticketName, $pdf); 

    //отправка на почту

    $mail = new PHPMailer;
    $mail->CharSet = "utf-8";
    $mail->setFrom('nwmtrain@gmail.com', 'RuTrain');
    $mail->addAddress($sendEmail, $sendName);
    $mail->Subject = 'Электронный билет с сайта RuTrain';
    $mail->msgHTML("Билет #".$genTicketName);
        // Attach uploaded files
    $mail->addAttachment($ticketName);
    $r = $mail->send();

    header('Location: ../index.php?page=index_profile');

?>
